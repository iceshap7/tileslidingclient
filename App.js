import React, { Component, Fragment } from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Alert,
  View,
  TouchableOpacity,
  Image
} from 'react-native';

export default class App extends Component {
  emptyTile = 3;
  cFlag = false;
  tried = 0;
  tiles = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0];
  clickCouter = 0;

  images = imageState = [
    require('./src/assets/images/cho-tu-te/1.png'),
    require('./src/assets/images/cho-tu-te/2.png'),
    require('./src/assets/images/cho-tu-te/3.png'),
    require('./src/assets/images/cho-tu-te/4.png'),
    require('./src/assets/images/cho-tu-te/5.png'),
    require('./src/assets/images/cho-tu-te/6.png'),
    require('./src/assets/images/cho-tu-te/7.png'),
    require('./src/assets/images/cho-tu-te/8.png'),
    require('./src/assets/images/cho-tu-te/9.png'),
    require('./src/assets/images/cho-tu-te/10.png'),
    require('./src/assets/images/cho-tu-te/11.png'),
    require('./src/assets/images/cho-tu-te/12.png'),
    require('./src/assets/images/cho-tu-te/13.png'),
    require('./src/assets/images/cho-tu-te/14.png'),
    require('./src/assets/images/cho-tu-te/15.png'),
    require('./src/assets/images/cho-tu-te/16.png')
  ]

  state = {
    images: Array.from(this.images)
  }

  tryMove(click) {
    if ((this.emptyTile - 1 == click && this.emptyTile % 4 !== 0)
      || (this.emptyTile + 1 == click && this.emptyTile % 4 !== 3)
      || this.emptyTile - 4 == click
      || this.emptyTile + 4 == click) {
      this.swap(this.emptyTile, click);
      this.emptyTile = click;
      this.check();
      return;
    }
  };

  shuffle() {
    this.clickCouter = 0;
    this.cFlag = false;
    while (this.tried < 200) {
      this.tryMove(Math.round((Math.random() * 15)));
    }

    this.setState(() => (
      state = {
        images: this.imageState
      }
    ));

    this.tried = 0;
    this.cFlag = true;
  }

  swap(a, b) {
    let cppArr = this.state.images;
    let temp = cppArr[a];
    cppArr[a] = cppArr[b];
    cppArr[b] = temp;

    this.imageState = cppArr;

    if(this.cFlag) {
      this.setState(() => (
        state = {
          images: cppArr
        }
      ));
    }

    tmp = this.tiles[a];
    this.tiles[a] = this.tiles[b];
    this.tiles[b] = tmp;
  }

  check() {
    let count = 0;

    if (!this.cFlag) {
      this.tried++;
      return;
    }

    this.clickCouter++;

    for (var i = 0; i < 15; i++) {
      if (this.tiles[i] == (i + 1)) count++;
    }

    if (count >= 15) {
      this.cFlag = false;
      Alert.alert(
        ' 🤩Well done🤩',
        'Clear by ' + this.clickCouter + ' moves',
        [
          {
            text: 'Close',
            style: 'cancel'
          },
          { text: 'Try my best', onPress: () => this.shuffle() },
        ],
        { cancelable: false }
      );
    }
  }

  componentDidMount() {
    this.shuffle();
  }

  render() {
    return (
      <Fragment>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView>
          <View style={ styles.container }>
            <View style={ [styles.games, styles.preview] }>
              {
                this.images.map((item, key) => (
                  <Image key={ key } source={ item } style={ styles.previewImage } />
                ))
              }
            </View>

            <View style={ styles.games} >
              {
                this.state.images.map((item, key) => {
                  return (
                    <TouchableOpacity key={ key } style={ styles.tile } activeOpacity={ 1 } onPress={ () => this.tryMove(key) }>
                      <Image source={ item } style={ styles.tileImage } />
                    </TouchableOpacity>
                  );
                })
              }
            </View>
          </View>
        </SafeAreaView>
      </Fragment>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingHorizontal: 20,
    paddingTop: 50,
    backgroundColor: '#6aa6a0'
  },

  games: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  },

  preview: {
    width: '50%',
    marginBottom: 30
  },

  previewImage: {
    width: '25%',
    aspectRatio: 1
  },

  tile: {
    aspectRatio: 1,
    width: '25%',
    alignSelf: 'flex-start',
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#fff'
  },

  tileImage: {
    width: '100%',
    height: '100%'
  }
});
